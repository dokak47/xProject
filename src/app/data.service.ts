import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  color: string;
  data: object = {
    code: 200,
    data: {},
    message: ''
  };

  constructor(
    public http: HttpClient
  ) {
  }

  initData() {
    // this.http.get('http://baidu.com')
    //   .toPromise()
    //   .then(res => {
    //     console.log(res);
    //   })
    //   .catch(err => {
    //   })
    // return Promise.resolve(this.data)
  }

  getColor() {
    return this.color;
  }

  setColor(str) {
    this.color = str;
    return this.color
  }
}
