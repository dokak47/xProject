/**
 * Created by 17825 on 2019/1/22.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'bottom-component',
  templateUrl: './bottom.html',
  styleUrls: ['./bottom.css']
})

export class BottomComponent {
  number = 7;
}
