/**
 * Created by 17825 on 2019/1/22.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'right-component',
  templateUrl: './right.html',
  styleUrls: ['./right.css']
})

export class RightComponent {
  number = 7;
}
