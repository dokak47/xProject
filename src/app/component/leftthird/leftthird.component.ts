/**
 * Created by 17825 on 2019/1/22.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'leftthird-component',
  templateUrl: './leftthird.html',
  styleUrls: ['./leftthird.css']
})
export class LeftthirdComponent {
  produceNum = '720';
  lostNum = '1450';
  moduleThirdGoal = 'Goal';
  chartOption = {
    toolbox: {
      feature : {
        magicType : {
          type: ['pie'],
        },
        // restore : {show: true},
      }
    },
    series : [
      {
        type : 'pie',
        hoverAnimation: false,
        radius : ['55%', '65%'],
        center : ['45%', '40%'],
        itemStyle : {
              normal : {
                label : {
                  position: 'center',
                  textStyle: {
                    color: '#000',
                    baseline: 'bottom',
                    fontSize: '18',
                    fontWeight: 'bolder'
                  },
                  formatter: function (params) {
                    if (params.name) {
                      if (params.name === 'produceNum')
                        return (parseInt((params.value / 1450 *100).toString())) + '%';
                      else return '';
                    }
                  }
                }
             },
        },
        data: [
          {value: 730, name: 'lostNum'},
          {value: 720, name: 'produceNum'},
        ]
      }
    ],
    color: [ '#c0141a', '#ddd']
  };
}
