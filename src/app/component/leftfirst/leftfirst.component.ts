/**
 * Created by 17825 on 2019/1/22.
 */
import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'leftfirst-component',
  templateUrl: './leftfirst.html',
  styleUrls: ['./leftfirst.css']
})

export class LeftfirstComponent {
  @Input() data: any = {}
  @Output() onSubmit = new EventEmitter<boolean>();
  moduleFirstNum1 = 'Assembly';
  moduleFirstNum2 = 'Cell 101';

  constructor(){

  }

  OnSubmit() {
    this.onSubmit.emit();
  }
}
