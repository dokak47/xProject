/**
 * Created by 17825 on 2019/1/22.
 */
import {Component, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {DataService} from "../../data.service";

@Component({
  selector: 'leftfourth-component',
  templateUrl: './leftfourth.html',
  styleUrls: ['./leftfourth.css']
})

export class LeftfourthComponent implements OnInit, OnDestroy, OnChanges {
  moduleFourthNums = [
    {
      name:'MICRO STOPS',
      time1:'2s',
      time2:'29ms',
      tit:'Reason',
      con:'Cerractive Action···',
    },
    {
      name:'DOWNTIME',
      time1:'',
      time2:'46ms',
      tit:'Mainrenance',
      con:'Repiace Sentor',
    },
    {
      name:'SLOW RUNNING',
      time1:'',
      time2:'29ms',
      tit:'Trairing',
      con:'. . .',
    }
  ];
  constructor (
    public dataService:DataService
  ){}

  ngOnInit() {
    setTimeout(()=>{
      console.log(444,this.dataService.initData());
    })
  }

  ngOnDestroy() {

  }

  ngOnChanges() {

  }
}
