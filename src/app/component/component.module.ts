import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';
registerLocaleData(zh);
// import { ImesMainRoutingModule } from '../imes-main/imes-main.routing.module';

import { RightComponent } from './right/right.component';
import { BottomComponent } from './bottom/bottom.component';
import { LeftfirstComponent } from './leftfirst/leftfirst.component';
import { LeftsecondComponent } from './leftsecond/leftsecond.component';
import { LeftthirdComponent } from './leftthird/leftthird.component';
import { LeftfourthComponent } from './leftfourth/leftfourth.component';
import { ProductionDirective } from './production.directive';

@NgModule({
  imports: [
    CommonModule,
    // ImesMainRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    RightComponent,
    BottomComponent,
    LeftfirstComponent,
    LeftsecondComponent,
    LeftthirdComponent,
    LeftfourthComponent,
    ProductionDirective
  ],
  exports: [
    RightComponent,
    BottomComponent,
    LeftfirstComponent,
    LeftsecondComponent,
    LeftthirdComponent,
    LeftfourthComponent,
    ProductionDirective
  ]
})

export class ComponentModule { }


