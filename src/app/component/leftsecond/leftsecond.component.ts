/**
 * Created by 17825 on 2019/1/22.
 */
import {
  Component,
  ElementRef,
  ViewChild,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from '@angular/core';
import {DataService} from "../../data.service";

@Component({
  selector: 'leftsecond-component',
  templateUrl: './leftsecond.html',
  styleUrls: ['./leftsecond.css']
})

export class LeftsecondComponent implements OnInit, OnDestroy, OnChanges {
  moduleSecond_produce = 'Total Parts Produced';
  moduleSecond_lost = 'Total Parts Lost';
  //
  @ViewChild('leftSecond') leftSecond: ElementRef;

  constructor(
    public dataService:DataService
  ){

  }

  ngOnInit() {
    setTimeout(()=>{
      console.log(this.dataService.setColor('123'));
      console.log(this.dataService.getColor());
      console.log('leftSecond', this.leftSecond.nativeElement.clientWidth)
    })

  }

  ngOnDestroy() {

  }

  ngOnChanges() {

  }

}

