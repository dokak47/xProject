import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataService } from './data.service';

// import { ContentComponent } from './content/content.component';

import { ContentModule } from './content/content.module';
import { ComponentModule } from './component/component.module';

@NgModule({
  declarations: [
    AppComponent,
    // ContentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ContentModule,
    ComponentModule,
    HttpClientModule,
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
