import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ComponentModule } from '../component/component.module';
import { IndexComponent } from './index/index.component';

import { ContentRoutingModule } from './content.routing.module';
import { ImesMainDirective } from './imes-main.directive';
@NgModule({
  imports: [
    CommonModule,
    ComponentModule,
    BrowserAnimationsModule,
    ContentRoutingModule
  ],
  declarations: [
    IndexComponent,
    ImesMainDirective
  ],
  exports: [
    ImesMainDirective
  ],
  providers: [
  ],
})

export class ContentModule {}
